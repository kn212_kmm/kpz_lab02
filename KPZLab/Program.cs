﻿using KPZLab.Class.my_vector;
using KPZLab.Interface;
using KPZLab2;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;


namespace KPZLab2
{
    class Program
    {
        public static void Main()
        {
            Vector<int> arr = new Vector<int> { 1, 2, 3, 4, 5, 6, 7 };
            Vector<int> arr1 = new Vector<int> { 1, 2, 3, 4, 5, 6, 7 };

            var tmp = arr.GetLessAndGreater(4, 1);

            Console.WriteLine(tmp.Item1);
            Console.WriteLine(tmp.Item2);

            arr.Insert(100, 8);

            for (int i = 0; i < arr.GetSize(); i++)
            {
                Console.Write($"{arr[i]} ");
            }

        }
    }
}

