﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZLab.Interface
{
    namespace my_vector
    {
        internal interface IVector<T>
        {
            bool IsEmpty { get; }
            int GetSize();
            int GetUpperBound();
            void Remove();
            void Remove(int index);
            void Add(T value);
            void Insert(T value);
            void Insert(T value, int index);
        }
    }
}
