﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZLab.Interface
{
    namespace my_vector
    {
        internal interface IVectorCounter
        {
            int _realSize { get; set; }
            void SetRealSize(int value);
            int GetUpperBound();
        }
    }
}
