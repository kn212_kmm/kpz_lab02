﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZLab.Interface
{
    namespace my_vector
    {
        internal interface ICalc
        {
            (int, int) GetLessAndGreater(int valueToCompareMin, int valueToCompareMax);
        }
    }
}
