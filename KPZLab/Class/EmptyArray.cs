﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZLab.Class
{
    namespace my_vector
    {
        internal static class EmptyArray<T>
        {
            public static readonly T[] ValueN1 = new T[0];
            public static readonly T[,] ValueN2 = new T[0,0];
            public static readonly T[][] ValueZ = new T[0][];
        }
    }
}
