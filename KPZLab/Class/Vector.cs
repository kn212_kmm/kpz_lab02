using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using KPZLab.Interface.my_vector;

namespace KPZLab.Class
{
    namespace my_vector
    {
        
        internal sealed class Vector<T> : IVector<T>,
                                          IEnumerable<T>,
                                          ICloneable,
                                          ICalc
        {
            private T[]? _array;
            private VectorCounter Counter = new();

            public bool IsEmpty => _array == null;
            public T[] GetArray => _array ?? EmptyArray<T>.ValueN1;
            public T this[int i]
            {
                get
                {
                    if (_array != null) return _array[i];
                    else throw new ArgumentNullException("NULL!!!");
                }
                set
                {
                    if (_array != null) _array[i] = value;
                    else throw new ArgumentNullException("NULL!!!");
                }
            }

            public Vector() { }
            public Vector(T[]? array) => _array = array;
            public Vector(T value) => Add(value);
            public Vector(Vector<T> value) 
            { 
                _array = value._array;
                Counter = value.Counter;
            }

            public int GetSize() => Counter._realSize;
            public int GetUpperBound() => Counter.GetUpperBound();
            public void Add(T value)
            {
                if (_array == null) 
                    _array = new T[1];
                else 
                    Array.Resize(ref _array, Counter._realSize + 1);
                
                _array[Counter._realSize] = value;
                Counter._realSize++;
            }

            //IVectror
            public IEnumerator<T> GetEnumerator()
            {
                if (_array != null)
                    return _array.AsEnumerable().GetEnumerator();
                return Enumerable.Empty<T>().GetEnumerator();
            }
            public void Insert(T value) => Add(value);
            public void Insert(T value, int index)
            {
                if (index >= Counter._realSize)
                    Add(value);
                else
                {
                    Array.Resize(ref _array, Counter._realSize + 1);
                    Counter._realSize++;
                    for (int i = Counter._realSize - 1 ; i >= 0; i--)
                    {
                        (_array[i - 1], _array[i]) = (_array[i], _array[i - 1]);
                        if (i == index)
                        {
                            _array[i - 1] = value;
                            break;
                        }
                    }
                }
            }   
            public void Remove()
            {
                _array = null;
                Counter._realSize = 0;
            }
            public void Remove(int index)
            {
                if (_array == null)
                    return;
                _array = _array.Where((val, idx) => idx != index).ToArray();
                Counter._realSize--;
            }
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
           /// <summary>
           /// Оператор =
           /// </summary>
            public object Clone() => MemberwiseClone();
            public override string ToString()
            {
                if (_array != null)
                    return _array.Aggregate(string.Empty, (s, i) => s + i + " ");
                return "";
            }
            public (int, int) GetLessAndGreater(int valueToCompareMin, int valueToCompareMax)
            {
                if (_array != null && _array.GetType().GetElementType() == typeof(int))
                    return (_array.Count(val => Convert.ToInt32(val) < valueToCompareMin),  
                            _array.Count(val => Convert.ToInt32(val) > valueToCompareMax));
                return (0, 0);
            }
            internal static Vector<T> Union(Vector<T> first, Vector<T> second)
            {
                Vector<T> tmp = new() { _array = first.Union(second).ToArray() };
                tmp.Counter.SetRealSize(first.Counter._realSize + second.Counter._realSize - 1);
                return tmp;
            }
            internal static Vector<T> Exept(Vector<T> first, Vector<T> second)
            {
                Vector<T> tmp = new() { _array = first.Except(second).ToArray() };
                tmp.Counter.SetRealSize(tmp._array.Length);
                return tmp;
            }
            public void Reverse()
            {
                if (_array != null)
                    for (int i = 0; i < Counter._realSize / 2; i++)
                    {
                        int _reverse_i = Counter._realSize - 1 - i;
                        (_array[i], _array[_reverse_i]) = (_array[_reverse_i], _array[i]);
                    }
            }

            //operators
            public static Vector<T> operator +(Vector<T> left, Vector<T> right)
            {
                return Union(left, right);
            }
            public static Vector<T> operator -(Vector<T> left, Vector<T> right)
            {
                return Exept(left, right);
            }
            public static bool operator ==(Vector<T> left, Vector<T> right)
            {
                if (left.Counter._realSize != right.Counter._realSize)
                    return false;
                else if (left._array == null || right._array == null)
                    return false;
                else
                    return Enumerable.SequenceEqual(left._array, right._array);
            }
            public static bool operator !=(Vector<T> left, Vector<T> right)
            {
                return !(left == right);
            }
            public static bool operator>(Vector<T> left, Vector<T> right)
            {
                return left.Counter._realSize > right.Counter._realSize;
            }
            public static bool operator >=(Vector<T> left, Vector<T> right)
            {
                return left.Counter._realSize >= right.Counter._realSize;
            }
            public static bool operator <(Vector<T> left, Vector<T> right)
            {
                return !(left >= right);
            }
            public static bool operator <=(Vector<T> left, Vector<T> right)
            {
                return !(left > right);
            }
            public override bool Equals(object? obj)
            {
                if (ReferenceEquals(this, obj))
                {
                    return this == obj;
                }

                if (ReferenceEquals(obj, null))
                {
                    return false;
                }

                return obj.Equals(this);
            }
            public override int GetHashCode()
            {
                return GetHashCode();
            }
        }
    }
}
