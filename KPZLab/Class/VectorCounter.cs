﻿using KPZLab.Interface.my_vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZLab.Class
{
    namespace my_vector
    {
        internal class VectorCounter : IVectorCounter
        {
            private int _rS = 0;

            public int _realSize { get => _rS; set => _rS = value; }

            public int GetUpperBound() => _realSize - 1;
            public void SetRealSize(int value) => _rS = value;
        }
    }
}
